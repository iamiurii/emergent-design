#ifndef IPHONE_MATRIX_H
#define IPHONE_MATRIX_H

#include <vector>
#include <string>
#include <sstream>
#include "ScreenMatrix.h"

enum MatrixState {
	Empty,
	Loaded
};

class IphoneMatrix: public ScreenMatrix {
	public:
		IphoneMatrix(): state(Empty)
		{
		}

		std::string operator()(const std::vector<int>& data) {

			std::stringstream result;
			result << "";
			

			MatrixState tempState = Empty;

			if (!isEmpty(data)) {
				tempState = Loaded;
				matrix = data;
			}

			if (Loaded == state && Empty == tempState)
				process(result, matrix);

			state = tempState;

			return result.str();
		}
		~IphoneMatrix() {}
	private:

		std::vector<int> matrix;
		MatrixState state;
		static constexpr int m_size = 100;

		bool isEmpty(const std::vector<int>& data) {
			size_t actualSize = (m_size < data.size()) ? m_size : data.size();
			for (int i = 0; i < actualSize; ++i)
				if (0 != data.at(i))
					return false;
			return true;
		}
		void process(std::stringstream & result, const std::vector<int>& data) {
			size_t actualSize = (m_size < data.size()) ? m_size : data.size();
			int maxX = 0, minX = 11;
			int maxY = 0, minY = 11;
			for (int i = 0; i < actualSize; ++i) {
				int x = i % 10 + 1;
				int y = i / 10 + 1;
				if (data.at(i)) {
					minX = (x < minX)?x:minX;
					maxX = (x > maxX)?x:maxX;
					minY = (y < minY)?y:minY;
					maxY = (y > maxY)?y:maxY;
				}
			}
			result << "D(" << (minX + maxX)/2 << "," << (minY + maxY)/2 << ")";
		}
		void dump(const std::vector<int>& data) {
			for (auto & point : data)
				std::cout << point << " ";
			std::cout << std::endl;
		}
};

ScreenMatrix * createScreenMatrix() {
	return new IphoneMatrix();
}

#endif
