#include <vector>
#include <string>
#include <iostream>

#include <assert.h>

#include "IphoneMatrix.h"



struct Point {
	int x;
	int y;
	static std::string  str(Point &&point) {
		std::stringstream ss;
		ss << "D(" << point.x << "," << point.y << ")";
		return ss.str();
	}
};

class Matrix {
	public:
		static std::vector<int> create(std::vector<Point> && points) {
			std::vector<int> result(100);

			for (auto & point : points) {
				int position = (point.x - 1) + (point.y - 1) * 10;
				result.at(position) = 1;

			}
			return result;
		}

};

void dump(const std::vector<int> & matrix) {
	for (int i = 0; i < matrix.size(); ++i) {
		if (!(i%10))
			std::cout << std::endl;
		std::cout << matrix.at(i) << ",";
	}
	std::cout << std::endl;
}

void testAssert(std::string && expected, std::string && result) {
	std::cout << "Result: " << result << std::endl;
	std::cout << "Expected: " << expected << std::endl;
	assert( result == expected);
	std::cout << "SUCCESS" << std::endl;
}
	
void testShortVector(ScreenMatrix * matrix) {
	std::vector<int> empty(100);
	std::vector<int> shortVector(50);
	shortVector.at(49) = 1;
	testAssert(std::string(""), (*matrix)(empty));
	testAssert(std::string(""), (*matrix)(shortVector));
	testAssert(std::string("D(10,5)"), (*matrix)(empty));
	dump(shortVector);
}

void testLongVector(ScreenMatrix * matrix) {
	std::vector<int> empty(100);
	std::vector<int> longVector(500);
	longVector.at(32) = 1;
	longVector.at(102) = 1;
	longVector.at(257) = 1;
	longVector.at(499) = 1;
	testAssert(std::string(""), (*matrix)(empty));
	testAssert(std::string(""), (*matrix)(longVector));
	testAssert(std::string("D(3,4)"), (*matrix)(empty));
	dump(longVector);
}

void testLastMatrixAnalized(ScreenMatrix * matrix) {	

	testAssert(std::string(""), (*matrix)( Matrix::create({}) ) );
	testAssert(std::string(""), (*matrix)( Matrix::create({
					{4, 6},
					{3, 6},
					}) ) );
	testAssert(std::string(""), (*matrix)( Matrix::create({
					{8, 2},
					{7, 3},
					}) ) );
	testAssert(std::string(""), (*matrix)( Matrix::create({
					{2, 2},
					}) ) );
	testAssert(Point::str({2,2}), (*matrix)( Matrix::create({}) ) );
}

int main(int argc, char * argv[]) {
	ScreenMatrix * matrix =  createScreenMatrix();

	std::vector<int> vec1(100);
	std::vector<int> vec2(vec1);
	std::vector<int> vec3(vec1);
	std::vector<int> vec4(vec1);

	std::vector<int> longVector(230);

	vec2.at(3) = 1;
	vec3.at(99) = 1;

	vec4.at(99) = 1;
	vec4.at(20) = 1;
	
	longVector.at(229) = 1;
	
	dump(vec3);

	std::cout << (*matrix)(vec1) << std::endl;
	std::cout << (*matrix)(vec2) << std::endl;

	std::cout << (*matrix)(vec1) << std::endl;
	std::cout << (*matrix)(vec1) << std::endl;

	std::cout << (*matrix)(vec2) << std::endl;
	std::cout << (*matrix)(vec3) << std::endl;
	std::cout << (*matrix)(vec1) << std::endl;

	
	std::cout << (*matrix)(vec1) << std::endl;
	std::cout << (*matrix)(vec4) << std::endl;
	std::cout << (*matrix)(vec1) << std::endl;
	
	testShortVector(matrix);
	testLongVector(matrix);
	testLastMatrixAnalized(matrix);

	return 0;
}
