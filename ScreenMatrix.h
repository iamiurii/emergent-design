#ifndef SCREEN_MATRIX_H
#define SCREEN_MATRIX_H

#include <vector>
#include <string>

class ScreenMatrix {
	public:
		virtual std::string operator()(const std::vector<int>& data) = 0;
		virtual ~ScreenMatrix() {}
};

ScreenMatrix * createScreenMatrix();

#endif
